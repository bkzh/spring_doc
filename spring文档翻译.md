spring框架文档：
Part I. Spring框架概述
  Spring框架是一个轻量级的解决方案，是构建企业级应用程序的一站式商店。 但是，Spring是模块化的，允许你只使用那些需要的部分，而不需要把剩下的东西放进去。 您可以使用IoC容器，顶部有任何Web框架，但您也可以只使用Hibernate集成代码或JDBC抽象层。 Spring框架支持声明式事务管理，通过RMI或Web服务对您的逻辑进行远程访问，以及用于保存数据的各种选项。 它提供了一个全功能的MVC框架，使您能够将AOP透明地集成到您的软件中。
  
  Spring被设计成非侵入式的，这意味着你的域逻辑代码通常不依赖框架本身。 在你的集成层（比如数据访问层）中，会存在对数据访问技术和Spring库的一些依赖。 但是，应该很容易将这些依赖关系与其他代码库相隔离。
  
  本文档是Spring Framework功能的参考指南。 如果您对本文档有任何请求，意见或疑问，请将它们发布到用户邮件列表中。 有关框架本身的问题应该在StackOverflow上提出（请参阅https://spring.io/questions）。
  
  
  1. Spring入门
  本参考指南提供了有关Spring框架的详细信息。 它为所有功能提供了全面的文档，并提供了Spring包含的一些背景知识（如“依赖注入”）。
  
  如果您刚刚开始使用Spring，您可能希望通过创建基于Spring Boot的应用程序来开始使用Spring Framework。 Spring Boot提供了一种快速（并且有见地）的方式来创建一个可以生产的基于Spring的应用程序。 它基于Spring框架，主张约定而不是配置，旨在让您尽快启动并运行。
  
  您可以使用start.spring.io生成一个基本项目，或者按照“入门指南”中的一个指南进行操作，例如构建RESTful Web服务。 除了更易于理解，这些指南非常重视任务，其中大部分都基于Spring Boot。 他们还涵盖了您在解决特定问题时可能需要考虑的Spring组合中的其他项目。
  
  2. Spring框架简介
  
  Spring框架是一个Java平台，为开发Java应用程序提供全面的基础设施支持。Spring处理基础设施，以便您可以专注于您的应用程序。
  Spring使您能够从“普通Java对象”（POJO）构建应用程序，并将企业服务以非侵入方式应用于POJO。此功能适用于Java SE编程模型和全部和部分Java EE。
  
  作为应用程序开发人员，您可以从Spring平台中受益：
  - 在数据库事务中执行Java方法，而不必处理事务API。
  - 使本地Java方法成为HTTP端点，而不必处理Servlet API。
  - 使本地Java方法成为消息处理程序，而不必处理JMS API。
  - 使本地Java方法成为管理操作，而不必处理JMX API。
  
  
  
  2.1 依赖注入和控制反转
  Java应用程序是一个松散的术语，它将从受限制的嵌入式应用程序运行到n层服务器端企业应用程序，通常由协作形成应用程序的对象组成。 因此，应用程序中的对象相互依赖。
  
  虽然Java平台提供了丰富的应用程序开发功能，但它缺乏将基本构建块组织成一个整体的方法，将这一任务交给了架构师和开发人员。 尽管您可以使用诸如Factory，Abstract Factory，Builder，Decorator和Service Locator等设计模式来组合应用程序的各种类和对象实例，但这些模式仅仅是：给出名义的最佳实践， 模式做了什么，在哪里应用它，它解决的问题等等。 模式是您必须在您的应用程序中实现的最佳实践。
  
  Spring框架控制反转（IoC）组件解决了这个问题，它提供了一个将不同组件组合到一个完全可用的应用程序中的正式手段。 Spring框架将形式化的设计模式编码为可以集成到自己的应用程序中的一类对象。 许多组织和机构以这种方式使用Spring框架来构建健壮的，可维护的应用程序。
  
  “问题是，控制的哪个方面是颠倒的？” Martin Fowler在2004年在他的网站上提出了关于控制反转（IoC）的这个问题。Fowler建议重新命名这个原理以使其更易于理解，并提出了依赖注入。
  
  
  
  
  2.2 框架模块
  Spring Framework由约20个模块组成的特征组成。 这些模块分为核心容器，数据访问/集成，Web，AOP（面向方面编程），Instrumentation，Messaging和Test，如下图所示。
  Figure 2.1. Spring框架概述
  
  ![Spring Framework Runtime](/img/spring-overview.png)
  
  以下各节列出了每个功能的可用模块及其工件名称及其涵盖的主题。 工件名称与依赖管理工具中使用的工件ID相关联。
  
  2.2.1 核心容器
  核心容器由Spring-core，spring-beans，spring-context，spring-context-support和spring-expression（Spring Expression Language）模块组成。

  Spring-Core和Spring-beans模块提供了框架的基本部分，包括IoC和依赖注入功能。 BeanFactory是工厂模式的一个复杂的实现。它消除了对程序化单例的需求，并允许将实际程序逻辑中的依赖关系的配置和规范分离。

  Context（spring-context）模块建立在Core和Beans模块提供的可靠基础之上：它是以类似于JNDI注册表的框架样式访问对象的一种手段。 Context模块从Beans模块中继承了它的特性，并增加了对国际化（例如使用资源包），事件传播，资源加载以及例如Servlet容器的上下文透明创建的支持。 Context模块还支持Java EE功能，如EJB，JMX和基本远程处理。 ApplicationContext接口是Context模块的焦点。 spring-context-support提供了将常见的第三方库集成到Spring应用上下文如缓存（EhCache，Guava，JCache），邮件（JavaMail），调度（CommonJ，Quartz）和模板引擎（FreeMarker，JasperReports，Velocity） 。

  spring-expression模块为运行时查询和操作对象图提供了强大的表达式语言。它是JSP 2.1规范中规定的统一表达式语言（统一EL）的扩展。该语言支持设置和获取属性值，属性赋值，方法调用，访问数组的内容，集合和索引器，逻辑和算术运算符，命名变量以及从Spring的IoC容器中按名称检索对象。它还支持列表预测和选择以及常用列表聚合
  
  
  2.2.2 AOP and Instrumentation
  
  2.2.3 Messaging
  
  2.2.4 Data Access/Integration
  
  2.2.5 Web
  
  2.2.6 Test
  (ps:前面概述性的内容，太泛泛而谈了，一开始简单了解下，后面再回头看)
  